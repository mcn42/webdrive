/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mnilsen.webdrive.util.Utils;

/**
 *
 * @author michaeln
 */
public class Main {
    
    public static void main(String[] args) {
        Locale.setDefault(new Locale("en", "US"));
        TimeZone.setDefault(TimeZone.getTimeZone("America/New_York"));
        Logger.getLogger(Main.class.getCanonicalName()).log(Level.INFO, "Setting Time Zone: {0}", TimeZone.getDefault().getDisplayName());
        
        Utils.config(".");
        Runtime.getRuntime().addShutdownHook(new ShutdownRunner());
        
        try {
            PersistenceManager.getInstance().initialize();
        } catch (FileException ex) {
            System.exit(1);
        }
    }

    private static class ShutdownRunner extends Thread {
        @Override
        public void run() {
            super.run(); //To change body of generated methods, choose Tools | Templates.
        }       
    }
    
}
