/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive.util;

/**
 *
 * @author michaeln
 */
public enum AppProperty {
    BASE_GTFS_URL_PATTERN("http://datamine.mta.info/mta_esi.php?key=%s&feed_id=%s"),
    GTFS_POLLING_PERIOD("30"),
    GTFS_AUTHORIZATION_KEY_NYCT("<Insert valid auth key here>"),
    GTFS_AUTHORIZATION_KEY_MNR(""),
    LOG_DIRECTORY("logs"),
    SCREEN_WIDTH("900"),
    SCREEN_HEIGHT("820"), 
    DATABASE_NAME("gtfs_dash"),
    FEED_COLLECTION_NAME("feeds"),
    ERROR_COLLECTION_NAME("errors"),
    STATISTICS_COLLECTION_NAME("statistics"), 
    DB_HOSTNAME("localhost"), 
    DB_PORT("27017"), 
    SESSIONS_COLLECTION_NAME("sessions"),
    STALE_DATA_THRESHHOLD_SECS("120"), 
    WEB_SERVER_PORT("8001"), 
    ERROR_LIST_SIZE("10"), 
    ERROR_NOTIFICATION_THRESHHOLD("10"), 
    LATE_NOTIFICATION_THRESHHOLD("300"), 
    AWS_REGION("US_EAST_1"), 
    NOTIFICATION_SUPRESSION_MINUTES("15"), 
    NOTIFICATION_FROM_EMAIL("michael.nilsen@mtabsc.org"), 
    NOTIFICAION_TARGET_COLLECTION_NAME("notification_targets"), 
    NOTIFICATION_FILE_PATH("./notifications.csv"),
    SLACK_POST_URL("https://hooks.slack.com/services/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"),
    USE_SLACK("true"), 
    INCIDENTS_COLLECTION_NAME("incidents"), 
    USE_EMAIL("false"), 
    DATABASE_USER_NAME("dbuser"), 
    DATABASE_USER_PWD("Be@con42"),
    USE_DATABASE_SECURITY("false"),
    RETRY_SLEEP_SECONDS("5"), RETRY_MODE("true"), 
    SYSTEM_EMAIL_ADDRESS("system@mnilsen.net"), 
    SESSION_EXPIRATION_MINUTES("20"), 
    CLEANUP_PERIOD_SECS("60"), 
    STORAGE_ROOT_PATH("C:\\temp\\data");
    
    private final String defaultValue;
    
    private AppProperty(String defaultVal)
    {
        this.defaultValue = defaultVal;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
