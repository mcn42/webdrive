/*
 * Copyright Metropolitan Transportation Authority NY
 * All Rights Reserved
 */
package org.mnilsen.webdrive.util;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author mnilsen
 */
public class Utils {
    private static ScheduledExecutorService scheduledExecService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
    private static AppPropertyManager appProperties = null;
    private static String appDirectory = "";
    
    public static void config(String configPath)
    {
        appProperties = new AppPropertyManager(configPath + "/" + "dashboard.properties");
        appDirectory = configPath;
    }

    public static String getAppDirectory() {
        return appDirectory;
    }

    public static AppPropertyManager getAppProperties() {
        return appProperties;
    }
    
    public static int millisToMinutes(long millis)
    {
        return (int)TimeUnit.MILLISECONDS.toMinutes(millis);
    }

    
    public static String getUptimeString(long since) {
        long diff = System.currentTimeMillis() - since;
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return diffDays + " d, " + parseTimeNumbs(diffHours) + " h, " + parseTimeNumbs(diffMinutes) + " min, " + parseTimeNumbs(diffSeconds) + " sec";
    }

    private static String parseTimeNumbs(long time) {
        String timeString = time + "";
        if (timeString.length() < 2)
            timeString = "0" + time;
        return timeString;
    }
    
    private static DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG);
    public static String formatTimestamp(long ts) {
        return df.format(new Date(ts));
    }
    
    public static String formatTimestamp() {
        return df.format(new Date());
    }
    
    public static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ex) {
            // no op
        }
    }

    public static ScheduledExecutorService getScheduledExecService() {
        return scheduledExecService;
    }
    
    
}
