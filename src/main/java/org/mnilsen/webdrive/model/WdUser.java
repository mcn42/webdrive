/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author michaeln
 */
@Entity(name="WdUser")
@Table(name="wd_user")
@NamedQueries({
    @NamedQuery(name="WdUser.findByEmail",query="select o from WdUser o where o.userEmail = :email")
})
public class WdUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;    
    private String userName;
    @Column(unique=true)
    private String userEmail;
    private String hashedPassword;
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lastPasswordChange;
    private Integer uploadLimitMB = 2048;
    private Integer globalLimitGB = 8;

    public WdUser() {
    }

    public WdUser(String userName, String userEmail, String password, UserRole role) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.hashedPassword = password;
        this.userRole = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Date getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(Date lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    public Integer getUploadLimitMB() {
        return uploadLimitMB;
    }

    public void setUploadLimitMB(Integer uploadLimitMB) {
        this.uploadLimitMB = uploadLimitMB;
    }

    public Integer getGlobalLimitGB() {
        return globalLimitGB;
    }

    public void setGlobalLimitGB(Integer globalLimitGB) {
        this.globalLimitGB = globalLimitGB;
    }

    
}
