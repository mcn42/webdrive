/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive.model;

import java.util.Date;
import java.util.UUID;
import org.mnilsen.webdrive.model.WdUser;
import org.mnilsen.webdrive.util.AppProperty;
import org.mnilsen.webdrive.util.Utils;

/**
 *
 * @author MNILSEN
 */
public class UserSession {
    private final long expirationMillis = Utils.getAppProperties().getLong(AppProperty.SESSION_EXPIRATION_MINUTES) * 60 * 1000;
    private final WdUser user;
    private final String sessionId;
    private final Date loginTime;
    private Date lastUpdate;

    public UserSession(WdUser user) {
        this.user = user;
        this.loginTime = new Date();
        this.lastUpdate = new Date();
        this.sessionId = UUID.randomUUID().toString();
    }

    public WdUser getUser() {
        return user;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
    
    public void update() {
        this.lastUpdate = new Date();
    }
    
    public boolean isExpired() {
        return (System.currentTimeMillis() - this.loginTime.getTime()) > this.expirationMillis;
    }
}
