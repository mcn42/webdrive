/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive.model;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author michaeln
 */
@Entity(name="WdFile")
@Table(name="wd_file")
@NamedQueries({
    @NamedQuery(name="WdFile.findByPath",query="select o from WdFile o where o.osPath = :path")
})
public class WdFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    private String osPath;
    private String name;
    private Boolean root;
    private Boolean directory;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date created;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date modified;
    @Enumerated(EnumType.STRING)
    private ShareScope shareScope = ShareScope.PRIVATE;
    private String shareUuid;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date shareExpiration = null;
    
    @ManyToOne()
    @JoinColumn(name="owner",referencedColumnName="id")
    private WdUser owner;

    public WdFile() {
    }
    
    public WdFile(Long id, WdUser owner, String path) {
        this.id = id;
        this.owner = owner;
        this.osPath = path;
    }

    public WdFile(String path) {
        this.osPath = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return new File(this.osPath);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public WdUser getOwner() {
        return owner;
    }

    public void setOwner(WdUser owner) {
        this.owner = owner;
    }

    public void setRoot(Boolean root) {
        this.root = root;
    }

    public String getOsPath() {
        return osPath;
    }

    public void setOsPath(String osPath) {
        this.osPath = osPath;
    }

    public ShareScope getShareScope() {
        return shareScope;
    }

    public void setShareScope(ShareScope shareScope) {
        this.shareScope = shareScope;
    }

    public String getShareUuid() {
        return shareUuid;
    }

    public void setShareUuid(String shareUuid) {
        this.shareUuid = shareUuid;
    }

    public void setDirectory(Boolean directory) {
        this.directory = directory;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getShareExpiration() {
        return shareExpiration;
    }

    public void setShareExpiration(Date shareExpiration) {
        this.shareExpiration = shareExpiration;
    }

    public Boolean getRoot() {
        return root;
    }

    public Boolean getDirectory() {
        return directory;
    }

}
