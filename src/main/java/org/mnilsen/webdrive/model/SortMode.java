/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive.model;

import javax.xml.bind.annotation.XmlEnum;

/**
 *
 * @author michaeln
 */
@XmlEnum(String.class)
public enum SortMode {
    NAME, CREATED, MODIFIED, TYPE, PUBLIC, SHARED;
}
