/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import org.mnilsen.webdrive.model.UserRole;
import org.mnilsen.webdrive.model.WdFile;
import org.mnilsen.webdrive.model.WdUser;
import org.mnilsen.webdrive.util.AppProperty;
import org.mnilsen.webdrive.util.Log;
import org.mnilsen.webdrive.util.Utils;

/**
 *
 * @author MNILSEN
 */
public class PersistenceManager {

    private static final PersistenceManager instance = new PersistenceManager();
    private final String systemEmailAddress = Utils.getAppProperties().get(AppProperty.SYSTEM_EMAIL_ADDRESS);
    private EntityManager em;

    public PersistenceManager() {
    }

    public static PersistenceManager getInstance() {
        return instance;
    }

    public final void initialize() throws FileException {
        Log.getLog().info("Initializing JPA persistence...");
        try {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("WebDrive_PU");
            this.em = factory.createEntityManager();
            Log.getLog().info("JPA persistence initialized.");           
        } catch (Exception ex) {
            Log.getLog().log(Level.SEVERE, "Could not initialize PersistenceManager", ex);
            throw new FileException("Could not initialize PersistenceManager", ex);
        }
        //  data initialization
        this.addSystemUser();
    }

    public WdUser getWdUserForEmail(String email) throws FileException {
        WdUser user = null;
        try {
            TypedQuery<WdUser> q = em.createNamedQuery("WdUser.findByEmail", WdUser.class).setParameter("email", email);
            user = q.getSingleResult();
        } catch (NoResultException ex) {
            Log.getLog().log(Level.WARNING, "Could not find WdUser record for email {0}", email);
        } catch (Exception ex) {
            Log.getLog().log(Level.SEVERE, "Could not retrieve WdUser record", ex);
            throw new FileException("Could not retrieve WdUser record", ex);
        }
        return user;
    }

    public WdUser addWdUser(WdUser user) throws FileException {
        try {
            this.em.getTransaction().begin();
            this.em.persist(user);
            this.em.getTransaction().commit();
        } catch (Exception ex) {
            this.em.getTransaction().rollback();
            Log.getLog().log(Level.SEVERE, "Could not persist WdUser record", ex);
            throw new FileException("Could not persist WdUser record", ex);
        }
        return user;
    }

    public WdUser updateWdUser(WdUser user) throws FileException {
        try {
            this.em.getTransaction().begin();
            user = this.em.merge(user);
            this.em.getTransaction().commit();
        } catch (Exception ex) {
            this.em.getTransaction().rollback();
            Log.getLog().log(Level.SEVERE, "Could not update WdUser record", ex);
            throw new FileException("Could not update WdUser record", ex);
        }
        return user;
    }

    public WdFile getWdFileForPath(String path) throws FileException {
        WdFile file = null;
        try {
            TypedQuery<WdFile> q = em.createNamedQuery("WdFile.findByPath", WdFile.class).setParameter("path", path);
            file = q.getSingleResult();
        } catch (NoResultException ex) {
            Log.getLog().log(Level.WARNING, "Could not find WdFile record for path {0}", path);
        } catch (Exception ex) {
            Log.getLog().log(Level.SEVERE, "Could not retrieve WdFile record", ex);
            throw new FileException("Could not retrieve WdFile record", ex);
        }
        return file;
    }

    public WdFile addWdFile(WdFile file) throws FileException {
        try {
            this.em.getTransaction().begin();
            this.em.persist(file);
            this.em.getTransaction().commit();
        } catch (Exception ex) {
            this.em.getTransaction().rollback();
            Log.getLog().log(Level.SEVERE, "Could not persist WdFile record", ex);
            throw new FileException("Could not persist WdFile record", ex);
        }
        return file;
    }

    public WdFile updateWdFile(WdFile file) throws FileException {
        try {
            this.em.getTransaction().begin();
            file = this.em.merge(file);
            this.em.getTransaction().commit();
        } catch (Exception ex) {
            this.em.getTransaction().rollback();
            Log.getLog().log(Level.SEVERE, "Could not update WdFile record", ex);
            throw new FileException("Could not update WdFile record", ex);
        }
        return file;
    }
    
    public WdUser addSystemUser() throws FileException {
        WdUser user = this.getWdUserForEmail(systemEmailAddress);
        if(user != null) {
            Log.getLog().log(Level.INFO, "System User with ID = {0} already exists", user.getId());
            return user;
        }
        user = new WdUser();
        user.setLastPasswordChange(new Date());
        user.setUserName("System User");
        user.setUserEmail(this.systemEmailAddress);
        String token = AuthenticationManager.getInstance().hash("sysdevl1".toCharArray());
        user.setHashedPassword(token);
        user.setUserRole(UserRole.USER);        
        user = this.addWdUser(user); 
        String msg = String.format("Added System user with Id=%s, token='%s'", user.getId(),user.getHashedPassword());
        Log.getLog().log(Level.INFO, msg);
        return user;
    }
}
