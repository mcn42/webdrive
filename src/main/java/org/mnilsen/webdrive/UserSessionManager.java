/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import org.mnilsen.webdrive.model.UserSession;
import org.mnilsen.webdrive.model.WdUser;
import org.mnilsen.webdrive.util.Log;

/**
 *
 * @author MNILSEN
 */
public class UserSessionManager {
    private static final UserSessionManager instance = new UserSessionManager();
    
    private final AtomicReference<Map<String,UserSession>> activeSessions = new AtomicReference<>(new HashMap<>());

    public UserSessionManager() {
    }

    public static UserSessionManager getInstance() {
        return instance;
    }
    
    public void addUserSession(WdUser user,UserSession session) {
        this.activeSessions.get().put(user.getUserEmail(), session);
        Log.getLog().info("Added session for user " + user.getUserEmail());
    }
    
    public boolean updateUserSession(WdUser user) {
        UserSession sess = this.activeSessions.get().get(user.getUserEmail());
        if(sess == null) {
            Log.getLog().warning(String.format("No active session found for update, user = %s", user.getUserEmail()));
            return false;
        }
        return true;
    }
    
    public void removeExpiredSessions() {
        Collection<UserSession> sessions = this.activeSessions.get().values();
        List<UserSession> toRemove =  sessions.stream().filter((s) -> s.isExpired()).collect(Collectors.toList());
        toRemove.forEach((s) -> this.activeSessions.get().remove(s.getUser().getUserEmail()));
    }
    
}
