/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.mnilsen.webdrive.model.WdFile;
import org.mnilsen.webdrive.util.AppProperty;
import org.mnilsen.webdrive.util.Log;
import org.mnilsen.webdrive.util.Utils;

/**
 *
 * @author michaeln
 */
public class WebDriveManager {

    private static final WebDriveManager instance = new WebDriveManager();

    private final int webServerPort = Utils.getAppProperties().getInt(AppProperty.WEB_SERVER_PORT);
    private final int cleanupPeriod = Utils.getAppProperties().getInt(AppProperty.CLEANUP_PERIOD_SECS);
    private final String storageRootPath = Utils.getAppProperties().get(AppProperty.STORAGE_ROOT_PATH);
    
    private WebServer webServer = new WebServer(webServerPort);

    private WdFile sharedDirectory = new WdFile();

    public WebDriveManager() {
        this.sharedDirectory.setCreated(new Date());
        this.sharedDirectory.setName("Shared Directory");
        String sharePath = this.storageRootPath + "/shared";
        this.sharedDirectory.setOsPath(sharePath);
        this.sharedDirectory.setRoot(Boolean.TRUE);
        this.sharedDirectory.setDirectory(Boolean.TRUE);
        File f = new File(sharePath);
        try {
            if (!f.exists()) {
                Log.getLog().info("Creating shared directory at " + sharePath);
                f.mkdirs();
            } else {
                
            }
        } catch (Exception ex) {
            Log.getLog().log(Level.SEVERE,"Could not create shared directory at " + sharePath,ex);
        }
    }

    public static WebDriveManager getInstance() {
        return instance;
    }

    public void start() {
        Utils.getScheduledExecService().scheduleAtFixedRate(
                () -> this.cleanup(), cleanupPeriod, cleanupPeriod, TimeUnit.SECONDS);
        this.webServer.startServer();
    }

    public void stop() {
        Utils.getScheduledExecService().shutdownNow();
        this.webServer.stop();
    }

    private void cleanup() {
        UserSessionManager.getInstance().removeExpiredSessions();
    }

    public String getStorageRootPath() {
        return storageRootPath;
    }

    public WdFile getSharedDirectory() {
        return sharedDirectory;
    }
    
    
}
