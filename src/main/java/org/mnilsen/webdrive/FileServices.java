/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.mnilsen.webdrive.model.SortMode;
import org.mnilsen.webdrive.model.WdFile;

/**
 *
 * @author michaeln
 */
public class FileServices {
    
    public List<WdFile> readDirectory(String path, String filter, SortMode sort) {
        File dir = new File(path);
        File[] files = dir.listFiles();
        List<WdFile> wdFiles = Arrays.stream(files).map((f) -> this.getOneFile(f)).collect(Collectors.toList());
        
        return wdFiles;
    }
    
    public WdFile getOneFile(File file) {
        WdFile wdf = new WdFile(file.getAbsolutePath());
        wdf.setDirectory(file.isDirectory());
        
        return wdf;
    }
    
    public InputStream getInputStream(String path) throws FileException {
        File f = new File(path);
        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException ex) {
            throw new FileException("Could not get InputStream for " + path,ex);
        }
    }
}
