/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.mnilsen.webdrive.model.WdUser;

/**
 *
 * @author MNILSEN
 */
public class AuthenticationManager {
    private static final AuthenticationManager instance = new AuthenticationManager(24);
    /**
     * Each token produced by this class uses this identifier as a prefix.
     */
    public static final String ID = "$31$";
    /**
     * The minimum recommended cost, used by default
     */
    public static final int DEFAULT_COST = 16;
    //private static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final String ALGORITHM = "PBKDF2WithHmacSHA512";
    private static final int SIZE = 128;
    private static final Pattern layout = Pattern.compile("\\$31\\$(\\d\\d?)\\$(.{43})");
    private final SecureRandom random;
    private final int cost;

    public AuthenticationManager(int cost) {
        iterations(cost);
        this.random = new SecureRandom();
        this.cost = cost;
    }

    private static int iterations(int cost) {
        if ((cost < 0) || (cost > 30)) {
            throw new IllegalArgumentException("cost: " + cost);
        }
        return 1 << cost;
    }

    public static AuthenticationManager getInstance() {
        return instance;
    }

    /**
     * Hash a password for storage.
     *
     * @return a secure authentication token to be stored for later
     * authentication
     */
    public String hash(char[] password) {
        byte[] salt = new byte[SIZE / 8];
        random.nextBytes(salt);
        byte[] dk = pbkdf2(password, salt, 1 << cost);
        byte[] hash = new byte[salt.length + dk.length];
        System.arraycopy(salt, 0, hash, 0, salt.length);
        System.arraycopy(dk, 0, hash, salt.length, dk.length);
        Base64.Encoder enc = Base64.getUrlEncoder().withoutPadding();
        return ID + cost + '$' + enc.encodeToString(hash);
    }

    /**
     * Authenticate with a password and a stored password token.
     *
     * @return true if the password and token match
     */
    public boolean authenticate(char[] password, String token) {
        Matcher m = layout.matcher(token);
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid token format");
        }
        int iterations = iterations(Integer.parseInt(m.group(1)));
        byte[] hash = Base64.getUrlDecoder().decode(m.group(2));
        byte[] salt = Arrays.copyOfRange(hash, 0, SIZE / 8);
        byte[] check = pbkdf2(password, salt, iterations);
        int zero = 0;
        for (int idx = 0; idx < check.length; ++idx) {
            zero |= hash[salt.length + idx] ^ check[idx];
        }
        return zero == 0;
    }

    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations) {
        KeySpec spec = new PBEKeySpec(password, salt, iterations, SIZE);
        try {
            SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
            return f.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException("Missing algorithm: " + ALGORITHM, ex);
        } catch (InvalidKeySpecException ex) {
            throw new IllegalStateException("Invalid SecretKeyFactory", ex);
        }
    }
    
    public boolean authenticate(WdUser user, String password) {
        boolean auth = this.authenticate(password.toCharArray(), user.getHashedPassword());
        return auth;
    }

    public WdUser setUserPassword(WdUser user, String password) {
        user.setLastPasswordChange(new Date());
        String token = this.hash(password.toCharArray());
        user.setHashedPassword(token);
        try {
            user = PersistenceManager.getInstance().updateWdUser(user);
        } catch (FileException ex) {
            Logger.getLogger(AuthenticationManager.class.getName()).log(Level.SEVERE, "Could not set user password", ex);
        }
        return user;
    }
}
