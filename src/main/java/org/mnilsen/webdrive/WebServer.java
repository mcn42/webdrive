/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.webdrive;

import java.util.logging.Level;
import java.util.stream.Stream;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.mnilsen.webdrive.util.Log;

/**
 *
 * @author MNILSEN
 */
public class WebServer {
    private Server server = null;
    private final int appPort;// = Utils.getAppProperties().getInt(AppProperty.HTTP_PORT);

    public WebServer(int appPort) {
        this.appPort = appPort;
        this.configureServer();
    }

    private void configureServer() {
        server = new Server(this.appPort);
        
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");
        context.setResourceBase("./webapp");
        context.setWelcomeFiles(new String[]{"index.html"});
        
                
        ServletHolder jerseyServlet = 
                new ServletHolder("rest",org.glassfish.jersey.servlet.ServletContainer.class);
        jerseyServlet.setInitOrder(0);

        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages",
                "org.mnilsen.webdrive.rest");
        context.addServlet(jerseyServlet, "/rest/*");
        
        ServletHolder deflt = new ServletHolder("default",DefaultServlet.class);
        deflt.setInitParameter("dirAllowed", "true");
        context.addServlet(deflt, "/");

        server.setHandler(context);
        
        //  Disable Server Version Header
        Stream.of(server.getConnectors()).flatMap(connector -> connector.getConnectionFactories().stream())
            .filter(connFactory -> connFactory instanceof HttpConnectionFactory)
            .forEach(httpConnFactory -> ((HttpConnectionFactory)httpConnFactory).getHttpConfiguration().setSendServerVersion(false));
    }

    public void startServer() {
        try {
            Log.getLog().info("Starting Web Server...");
            server.start();
            Thread.sleep(10000);
            server.join();

        } catch (Exception ex) {
            Log.getLog().log(Level.SEVERE, "The Web Server failed to start", ex);
        }
        Log.getLog().info("The Web Server has started successfully");
    }

    public void stop() {
        if (this.server != null && this.server.isRunning()) {
            try {
                this.server.stop();
            } catch (Exception ex) {
                Log.getLog().log(Level.SEVERE, "Web server shutdown error", ex);
            }
        }
    }
}
