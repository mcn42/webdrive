
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.mnilsen.webdrive.model.UserSession;
import org.mnilsen.webdrive.util.AppProperty;
import org.mnilsen.webdrive.util.Log;
import org.mnilsen.webdrive.util.Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MNILSEN
 */
public class SessionManager {

    private static final SessionManager instance = new SessionManager();
    private final long expirationMillis = Utils.getAppProperties().getLong(AppProperty.SESSION_EXPIRATION_MINUTES) * 60 * 1000;
    private final AtomicReference<Map<String, UserSession>> sessions = new AtomicReference<>(new HashMap<>());

    public SessionManager() {
    }

    public static SessionManager getInstance() {
        return instance;
    }

    public synchronized void addSession(UserSession sess) {
        String email = sess.getUser().getUserEmail();
        if (this.sessions.get().containsKey(email)) {
            Log.getLog().warning("Session already exists for user " + email);
            return;
        }
        this.sessions.get().put(email, sess);
        Log.getLog().info("Added user session for " + email);
    }

    public synchronized void removeSession(UserSession sess) {
        String email = sess.getUser().getUserEmail();
        UserSession us = this.sessions.get().remove(email);
        if (us == null) {
            Log.getLog().warning("Session not found for removal for user " + email);
        } else {
            Log.getLog().warning("Session removed for user " + email);
        }
    }
    
    private void cleanUpSessions() {
        
    }

}
